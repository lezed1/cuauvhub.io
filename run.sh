#!/usr/bin/env bash

docker run --rm \
       --volume "$PWD:/srv/jekyll" \
       --volume="$PWD/vendor/bundle:/usr/local/bundle" \
       --publish 4000:4000 \
       --publish 35729:35729 \
       --name cuauv.github.io \
       -it jekyll/jekyll \
       jekyll serve --incremental --watch --livereload
